
Please make sure maplotlib is installed on your computer.
For now the scripts are written in Python3.



* neural_net_matmtazur_tutorial.py  :   That's like Matt Mazur's tutorial as is coded in Python
* simple_neural_net_with_plot.py    :   My modified version that loops through as many 
                                        steps as you want and displays a plot of the outputs and errors
                                        of the neural network at each step.

I'm putting all this under GPL3.0 licence aka just add me in the credits.

Naysan

