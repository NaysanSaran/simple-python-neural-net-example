"""
 Author : Naysan Saran, June 2016

 Description :
    This python script is strongly inspired
    by Matt Mazur's "Step by Step Backpropagation Example"
    https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/

    It implements the same neural network as in the tutorial.
    I make it loop through 1000 steps by default before
    drawing a plot of how the total error is minimmized.

    Licence : GPL V3.0
"""


import numpy as np
import matplotlib.pyplot as plt


# Number of steps
number_steps = 1000

#
# Input values
#
i1 = 0.05
i2 = 0.10

#
# Target output values
#
target_o1 = 0.01
target_o2 = 0.99


class NeuralNetwork():

    def __init__(self):
        # Set the defaults values for
        # each weight and bias
        self.w1 = 0.15
        self.w2 = 0.20
        self.w3 = 0.25
        self.w4 = 0.30
        self.w5 = 0.40
        self.w6 = 0.45
        self.w7 = 0.50
        self.w8 = 0.55
        self.b1 = 0.35
        self.b2 = 0.60
        self.learning_rate = 0.5

    def sigmoid(self, val):
        # Apply the sigmoid activation function
        return 1.0 / ( 1.0 + np.exp(-val))


    #
    # All the steps required to propagate the inputs
    # must be called in the same order as they are
    # declared here.
    #
    def compute_net_h1(self) :
        """ Net inputs for the first hidden neuron """
        self.net_h1 = i1 * self.w1 + i2 * self.w2 + self.b1

    def compute_net_h2(self) :
        """ Net inputs for the second hidden neuron """
        self.net_h2 = i1 * self.w3 + i2 * self.w4 + self.b1

    def compute_out_h1(self) :
        """ Output value for the first hidden neuron """
        self.out_h1 = self.sigmoid(self.net_h1)

    def compute_out_h2(self) :
        """ Output value for the second hidden neuron """
        self.out_h2 = self.sigmoid(self.net_h2)

    def compute_net_o1(self) :
        """ Net inputs for the first output """
        self.net_o1 = self.out_h1 * self.w5 + self.out_h2 * self.w6 + self.b2

    def compute_net_o2(self) :
        """ Net inputs for the second output """
        self.net_o2 = self.out_h1 * self.w7 + self.out_h2 * self.w8 + self.b2

    def compute_out_o1(self) :
        """ First output value """
        self.out_o1 = self.sigmoid(self.net_o1)

    def compute_out_o2(self) :
        """ Second output value """
        self.out_o2 = self.sigmoid(self.net_o2)


    #
    # Each call to "forward" will be a step closer
    # to finding the minimum of the error function.
    #
    def forward(self):
        """ Propagate the inputs trough the network """
        self.compute_net_h1()
        self.compute_net_h2()

        self.compute_out_h1()
        self.compute_out_h2()

        self.compute_net_o1()
        self.compute_net_o2()

        self.compute_out_o1()
        self.compute_out_o2()


    def compute_error(self):
        """
            Computes the error associated with
            the current outputs.
            Should be called after each call
            to "self.forward()"
        """
        self.err_o1 = 0.5 * (target_o1 - self.out_o1)**2
        self.err_o2 = 0.5 * (target_o2 - self.out_o2)**2
        self.total_error = self.err_o1 + self.err_o2


    #
    # These methods will compute all the partial
    # derivatives required to know which way to go
    # to minimize the total error.
    #
    def set_dEtotaldOuto1(self):
        self.dEtotaldOuto1 = self.out_o1 - target_o1
    def set_dEtotaldOuto2(self):
        self.dEtotaldOuto2 = self.out_o2 - target_o2


    def set_dOuto1dNeto1(self):
        self.dOuto1dNeto1 = self.out_o1 * (1 - self.out_o1)
    def set_dOuto2dNeto2(self):
        self.dOuto2dNeto2 = self.out_o2 * (1 - self.out_o2)


    def set_dNeto1dw5(self):
        self.dNeto1dw5 = self.out_h1
    def set_dNeto1dw6(self):
        self.dNeto1dw6 = self.out_h2
    def set_dNeto2dw7(self):
        self.dNeto2dw7 = self.out_h1
    def set_dNeto2dw8(self):
        self.dNeto2dw8 = self.out_h2
    def set_dNeth1dw1(self):
        self.dNeth1dw1 = i1
    def set_dNeth1dw2(self):
        self.dNeth1dw2 = i2
    def set_dNeth2dw3(self):
        self.dNeth2dw3 = i1
    def set_dNeth2dw4(self):
        self.dNeth2dw4 = i2


    def set_dNeto1dOuth1(self):
        self.dNeto1dOuth1 = self.w5
    def set_dNeto2dOuth1(self):
        self.dNeto2dOuth1 = self.w7
    def set_dNeto1dOuth2(self):
        self.dNeto1dOuth2 = self.w6
    def set_dNeto2dOuth2(self):
        self.dNeto2dOuth2 = self.w8


    def set_dOuth1dNeth1(self):
        self.dOuth1dNeth1 = self.out_h1 * (1 - self.out_h1)
    def set_dOuth2dNeth2(self):
        self.dOuth2dNeth2 = self.out_h2 * (1 - self.out_h2)


    def set_dEtotaldOuth1(self):
        self.dEtotaldOuth1 = self.dEtotaldOuto1 * self.dOuto1dNeto1 * \
                             self.dNeto1dOuth1 \
                                    + \
                             self.dEtotaldOuto2 * self.dOuto2dNeto2 * \
                             self.dNeto2dOuth1
    def set_dEtotaldOuth2(self):
        self.dEtotaldOuth2 = self.dEtotaldOuto1 * self.dOuto1dNeto1 * \
                             self.dNeto1dOuth2 \
                                    + \
                             self.dEtotaldOuto2 * self.dOuto2dNeto2 * \
                             self.dNeto2dOuth2


    def set_dEtotaldw5(self):
        self.dEtotaldw5 = self.dEtotaldOuto1 * self.dOuto1dNeto1 * self.dNeto1dw5
    def set_dEtotaldw6(self):
        self.dEtotaldw6 = self.dEtotaldOuto1 * self.dOuto1dNeto1 * self.dNeto1dw6
    def set_dEtotaldw7(self):
        self.dEtotaldw7 = self.dEtotaldOuto2 * self.dOuto2dNeto2 * self.dNeto2dw7
    def set_dEtotaldw8(self):
        self.dEtotaldw8 = self.dEtotaldOuto2 * self.dOuto2dNeto2 * self.dNeto2dw8

    def set_dEtotaldw1(self):
        self.dEtotaldw1 = self.dEtotaldOuth1 * self.dOuth1dNeth1 * self.dNeth1dw1
    def set_dEtotaldw2(self):
        self.dEtotaldw2 = self.dEtotaldOuth1 * self.dOuth1dNeth1 * self.dNeth1dw2
    def set_dEtotaldw3(self):
        self.dEtotaldw3 = self.dEtotaldOuth2 * self.dOuth2dNeth2 * self.dNeth2dw3
    def set_dEtotaldw4(self):
        self.dEtotaldw4 = self.dEtotaldOuth2 * self.dOuth2dNeth2 * self.dNeth2dw4



    def set_partial_derivatives(self):
        """
            This one will compute the partial derivatives in
            an order that will respect the dependencies
            between each formula.
        """
        self.set_dEtotaldOuto1()
        self.set_dEtotaldOuto2()

        self.set_dOuto1dNeto1()
        self.set_dOuto2dNeto2()

        self.set_dNeto1dw5()
        self.set_dNeto1dw6()
        self.set_dNeto2dw7()
        self.set_dNeto2dw8()
        self.set_dNeth1dw1()
        self.set_dNeth1dw2()
        self.set_dNeth2dw3()
        self.set_dNeth2dw4()

        self.set_dNeto1dOuth1()
        self.set_dNeto2dOuth1()
        self.set_dNeto1dOuth2()
        self.set_dNeto2dOuth2()

        self.set_dOuth1dNeth1()
        self.set_dOuth2dNeth2()

        self.set_dEtotaldOuth1()
        self.set_dEtotaldOuth2()

        self.set_dEtotaldw5()
        self.set_dEtotaldw6()
        self.set_dEtotaldw7()
        self.set_dEtotaldw8()

        self.set_dEtotaldw1()
        self.set_dEtotaldw2()
        self.set_dEtotaldw3()
        self.set_dEtotaldw4()


    def compute_new_weights(self):
        """
            So, what's the next step in each
            direction ?
        """
        self.compute_error()
        self.set_partial_derivatives()

        self.new_w5 = self.w5 - self.learning_rate * self.dEtotaldw5
        self.new_w6 = self.w6 - self.learning_rate * self.dEtotaldw6
        self.new_w7 = self.w7 - self.learning_rate * self.dEtotaldw7
        self.new_w8 = self.w8 - self.learning_rate * self.dEtotaldw8
        self.new_w1 = self.w1 - self.learning_rate * self.dEtotaldw1
        self.new_w2 = self.w2 - self.learning_rate * self.dEtotaldw2
        self.new_w3 = self.w3 - self.learning_rate * self.dEtotaldw3
        self.new_w4 = self.w4 - self.learning_rate * self.dEtotaldw4

    def assign_new_weights(self):
        """
            Preparing the next forward() step ...
            The new adjusted weights will become the ones we
            are going to work with
        """
        self.w1 = self.new_w1
        self.w2 = self.new_w2
        self.w3 = self.new_w3
        self.w4 = self.new_w4
        self.w5 = self.new_w5
        self.w6 = self.new_w6
        self.w7 = self.new_w7
        self.w8 = self.new_w8


    def summarize_forward_step(self):
        """
            Provide a quick overview of the
            current forward step.
        """
        print("Weights      = {}".format([self.w1, self.w2, self.w3, self.w4, self.w5, self.w6, self.w7, self.w8]))
        print("Err o1       = {:.9f}".format(self.err_o1))
        print("Err o2       = {:.9f}".format(self.err_o2))
        print("Total error  = {:.9f}".format(self.total_error))


    def summarize_forward_step_verbose(self) :
        """
            Provide a (verbose) text summary of the key figures
            corresponding to the current forward iteration.
        """

        print("Net h1 = {:.9f}".format(self.net_h1))
        print("Net h2 = {:.9f}".format(self.net_h2))
        print("Out h1 = {:.9f}".format(self.out_h1))
        print("Out h2 = {:.9f}".format(self.out_h2))

        print("\n")

        print("Net o1 = {:.9f}".format(self.net_o1))
        print("Net o2 = {:.9f}".format(self.net_o2))
        print("Out o1 = {:.9f}".format(self.out_o1))
        print("Out o2 = {:.9f}".format(self.out_o2))

        print("\n")

        print("Err o1 = {:.9f}".format(self.err_o1))
        print("Err o2 = {:.9f}".format(self.err_o2))
        print("Total error = {:.9f}".format(self.total_error))

        print("\n")

        print("dEtotaldOuto1 = {:.9f}".format(self.dEtotaldOuto1))
        print("dOuto1dNeto1  = {:.9f}".format(self.dOuto1dNeto1))
        print("dNeto1dw5     = {:.9f}".format(self.dNeto1dw5))
        print("dEtotaldw5    = {:.9f}".format(self.dEtotaldw5))

        print("\n")

        print("New w1 = {:.9f}".format(self.new_w1))
        print("New w2 = {:.9f}".format(self.new_w2))
        print("New w3 = {:.9f}".format(self.new_w3))
        print("New w4 = {:.9f}".format(self.new_w4))
        print("New w5 = {:.9f}".format(self.new_w5))
        print("New w6 = {:.9f}".format(self.new_w6))
        print("New w7 = {:.9f}".format(self.new_w7))
        print("New w8 = {:.9f}".format(self.new_w8))


#----------------------------------------------------------
# New weights
#----------------------------------------------------------


def main():


    # Initialize the lists that we'll use to
    # draw our plot at the very end
    x_axis          = range(number_steps)
    target_error    = [0] * number_steps
    target_output1  = [target_o1] * number_steps
    target_output2  = [target_o2] * number_steps

    total_error     = []
    error_o1        = []
    error_o2        = []
    outputs_o1      = []
    outputs_o2      = []

    # Instanciate the neural network
    NN = NeuralNetwork()

    # Loop through as many steps as necessary
    for i in x_axis :
        NN.forward()
        NN.compute_new_weights()
        NN.summarize_forward_step()
        NN.assign_new_weights()

        # Don't forget to accumulate the data ...
        outputs_o1.append(NN.out_o1)
        outputs_o2.append(NN.out_o2)

        error_o1.append(NN.err_o1)
        error_o2.append(NN.err_o2)

        total_error.append(NN.total_error)


    #
    # Now we can plot our graphs
    #

    fig = plt.figure(figsize=(12,6))

    fig.suptitle("A graphical view of Matt Mazure's backpropagation tutorial", fontsize=14, fontweight='bold')

    #
    # Plot the values of the first output neuron
    #
    plt.subplot(221)
    plt.plot(x_axis, target_output1, label="Target for output #1")
    plt.plot(x_axis, outputs_o1, label="Actual value of output #1")

    plt.xlabel("Number of steps")
    plt.ylabel("Values of output neuron #1")

    plt.legend()
    plt.grid(True)

    #
    #  Plot the values of the second output neuron
    #
    plt.subplot(223)
    plt.plot(x_axis, target_output2, label="Target for output #2")
    plt.plot(x_axis, outputs_o2, label="Actual value of output #2")

    plt.xlabel("Number of steps")
    plt.ylabel("Values of output neuron #2")

    plt.legend()
    plt.grid(True)

    #
    #  Plot the graph of the errors
    #
    plt.subplot(122)
    plt.plot(x_axis, total_error, label="Total error of our Neural Network")
    plt.plot(x_axis, error_o1, label="Error of Output #1")
    plt.plot(x_axis, error_o2, label="Error of Output #2")
    plt.plot(x_axis, target_error, label="Perfect zero error line")

    plt.xlabel("Number of steps")
    plt.ylabel("Total error (to be minimized)")

    plt.legend()
    plt.grid(True)

    plt.title("Evolution of the error values")
    plt.savefig("sample_plot.png")
    plt.show()




if __name__ == "__main__" :
    main()


